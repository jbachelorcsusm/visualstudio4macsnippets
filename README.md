# Visual Studio For Mac Code Snippets #

This is simply personal collection of custom snippets I have created to make my life a little bit easier. 

### Where To Stick These On Your Mac
I have yet to find a way to change the path of where custom snippets are stored, and the path used by default appears dependent on the *version* of Visual Studio you are using. It is possible that this path may not be exactly the same on your Mac, but it should be something like: 

__~/Library/VisualStudio/7.0/Snippets__